import { test } from 'qunit';
import moduleForAcceptance from 't49-pil/tests/helpers/module-for-acceptance';

moduleForAcceptance('Acceptance | booking lookup');

test('Enter booking number, transition to detail page', function(assert) {
  const booking = server.create('booking');

  visit('/');

  fillIn('[data-test-input=booking-number-search]',
    booking.bookingNumber);

  click('[data-test-action=lookup-booking]');

  andThen(function() {
    assert.equal(currentURL(), `/bookings/${booking.bookingNumber}`);
  });
});

test('Enter unknown booking number, message is shown', function(assert) {
  visit('/');

  fillIn('[data-test-input=booking-number-search]',
    'unknown');

  click('[data-test-action=lookup-booking]');

  andThen(function() {
    assert.equal(
      find('[data-test-message=booking-lookup-error]').length, 1,
      "Displays message that booking could not be found");
  });
});
