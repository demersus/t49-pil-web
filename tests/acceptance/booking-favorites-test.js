import Ember from 'ember';
import { test } from 'qunit';
import moduleForAcceptance from 't49-pil/tests/helpers/module-for-acceptance';

moduleForAcceptance('Acceptance | booking favorites', {
  beforeEach() {
    const mockStorage = Ember.Service.extend({
      data: {},
      getItem(key) {
        return this.get('data')[key];
      },
      setItem(key, value) {
        this.get('data')[key] = value;
      }
    });

    this.application.register('service:mockStorage', mockStorage);
    this.application.inject('service:localSettings', 'storage', 'service:mockStorage');
  }
});

test('visiting /booking, add to favorites, remove from favorites', function(assert) {
  const booking = server.create('booking', {
    bookingNumber: "21312jk"
  });

  visit('/bookings/' + booking.bookingNumber);

  click('[data-test-action=add-booking-to-favorites]');

  visit('/');

  click(`[data-test-favorite=${booking.bookingNumber}]`);

  andThen(function() {
    assert.equal(
      currentURL(), '/bookings/' + booking.bookingNumber);
  });

  click('[data-test-action=remove-booking-from-favorites]');
  visit('/');

  andThen(function() {
    assert.equal(
      find(`[data-test-favorite=${booking.bookingNumber}]`).length, 0);
  });
});
