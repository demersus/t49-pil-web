import moment from 'moment'
import { test } from 'qunit';
import moduleForAcceptance from 't49-pil/tests/helpers/module-for-acceptance';

moduleForAcceptance('Acceptance | booking detail');

test('visiting /bookings/{booking-number} - Booking details', function(assert) {
  const booking = server.create('booking');

  visit(`/bookings/${booking.bookingNumber}`);

  andThen(function() {
    assert.equal(
      find('[data-test-display=booking-number]').text(), booking.bookingNumber,
      "Displays booking number");

    assert.equal(
      find('[data-test-display=steamship-line]').text(), booking.steamshipLine,
      "Displays steamship line");

    assert.equal(
      find('[data-test-display=origin]').text(), booking.placeOfReceipt,
      "Displays origin");

    assert.equal(
      find('[data-test-display=destination]').text(), booking.placeOfDelivery,
      "Displays destination");

    assert.equal(
      find('[data-test-display=vessel]').text(), booking.vessel,
      "Displays vessel");

    assert.equal(
      find('[data-test-display=voyage]').text(), booking.voyage,
      "Displays voyage");

    assert.equal(
      find('[data-test-display=eta]').text(), moment(new Date(booking.nextLocationDate)).format('MMM D, YYYY'),
      "Displays eta");
  });
});

test('visiting /bookings/{booking-number} - Containers', function(assert) {
  const booking = server.create('booking');
  const containers = server.createList('container', 2, {booking: booking});
  containers.forEach((container) => {
    server.createList('updates', 2, {
      bookingNumber: booking.bookingNumber,
      containerNumber: container.containerNumber
    });
  });

  visit(`/bookings/${booking.bookingNumber}`);

  andThen(()=>{
    containers.forEach((container, i) => {
      let resultPrefix = `Container ${i}`;
      let testScope = `[data-test-container=${container.containerNumber}]`;

      assert.equal(
        find('[data-test-display=container-number]', testScope).text().trim(),
        container.containerNumber,
        `Displays ${resultPrefix} container number`);

      assert.equal(
        find('[data-test-display=container-type]', testScope).text().trim(),
        container.containerType,
        `Displays ${resultPrefix} container type`);

      assert.equal(
        find('[data-test-display=place]', testScope).text().trim(),
        container.place,
        `Displays ${resultPrefix} place`);

      assert.equal(
        find('[data-test-display=latest-event]', testScope).text().trim(),
        container.latestEvent,
        `Displays ${resultPrefix} latest status`);

      assert.equal(
        find('[data-test-display=date]', testScope).text().trim(),
        moment(new Date(container.date)).format("L LTS"),
        `Displays ${resultPrefix} latest status date`);

      assert.equal(
        find(`[data-test-trace-for=${container.containerNumber}]`).length,
        0);

      click('[data-test-action=trace]', testScope);

      andThen(() => {
        assert.equal(
          find(`[data-test-trace-for=${container.containerNumber}]`).length,
          1, 'clicking the Trace button reveals updates for the container.');

      });
    });
  });
});
