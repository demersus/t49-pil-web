import { Factory } from 'ember-cli-mirage';

export default Factory.extend({
  containerNumber(n) {
    return "PCIU1857050" + n;
  },
  containerType: "20GP",
  movementType: "FCL/FCL",
  date: "2017-05-19 15:41:00",
  latestEvent: "Empty Container",
  place: "OAKLAND",

  afterCreate(container, server) {
    server.createList('update', 3, {
      containerNumber: container.containerNumber,
      bookingNumber: container.booking.bookingNumber
    });
  }
});
