import { Factory } from 'ember-cli-mirage';
import moment from 'moment';

export default Factory.extend({
  date(n) {
    return moment().subtract(n, 'h').format('YYYY-MM-DD HH:mm:ss');
  },
  latest_event: "Empty Release for outbound",
  booking_number: "TXG790195100",
  container_number: "PCIU1857050",
  place: "SEATTLE"
});
