import { Factory } from 'ember-cli-mirage';

export default Factory.extend({
  bookingNumber(n) {
    return "TXG790195100" + n;
  },
  steamshipLine: 'PIL',
  arrival: "2017-04-18",
  delivery: "2017-04-19",
  location: "Load Port XINGANG CNTXG",
  vessel: "CSCL AUTUMN",
  voyage: "VQC60007E",
  nextLocation: "USOAK",
  nextLocationDate: "2017-05-15",
  placeOfReceipt: "XINGANG [CNTXG]",
  placeOfDelivery: "OAKLAND [USOAK]",

  numberOfContainers: "1 x 20GP",

  afterCreate(booking, server) {
    booking.containers = booking.containers || server.createList('container', 3, {booking});
  }
});
