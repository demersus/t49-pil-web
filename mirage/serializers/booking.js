import {Serializer} from 'ember-cli-mirage';
import ApplicationSerializer from './application';

export default ApplicationSerializer.extend({
  serialize() {
    let json = Serializer.prototype.serialize.apply(this, arguments);

    delete json['id'] // real api doesn't have id
    return json;
  },

  include: ['containers'],
  embed: true
});
