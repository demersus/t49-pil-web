import { Serializer } from 'ember-cli-mirage';
import { ActiveModelSerializer } from 'ember-cli-mirage';

export default ActiveModelSerializer.extend({
  embed: true,

  serialize() {
    let json = Serializer.prototype.serialize.apply(this, arguments);

    delete json['id'];  // no traditional id in this json schema

    return json;
  }
});
