FROM node:6-alpine

RUN apk add --update --no-cache nginx git

RUN npm install -g ember-cli
RUN mkdir /app
ADD . /app
WORKDIR /app/
RUN npm install
RUN ember build --environment production

COPY nginx.conf /etc/nginx/nginx.conf

EXPOSE 80
STOPSIGNAL SIGTERM
CMD ["nginx", "-g", "daemon off;"]
