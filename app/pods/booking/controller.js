import Ember from 'ember';

const {
  Controller,
  get,
  computed,
  inject: {service}
} = Ember;

export default Controller.extend({
  localSettings: service(),
  inFavorites: computed('model.bookingNumber', 'localSettings.favorites.[]', function() {
    const bookingNum = get(this, 'model.bookingNumber');
    const favorites = get(this, 'localSettings.favorites');

    return favorites.includes(bookingNum);
  }),

  actions: {
    addToFavorites() {
      const bookingNum = get(this, 'model.bookingNumber');
      const settings = get(this, 'localSettings');

      settings.addBookingToFavorites(bookingNum);
    },
    removeFromFavorites() {
      const bookingNum = get(this, 'model.bookingNumber');
      const settings = get(this, 'localSettings');

      settings.removeBookingFromFavorites(bookingNum);
    }
  }
});
