import Controller from '@ember/controller';

export default Controller.extend({
  actions: {
    navigateToBooking(booking) {
      this.transitionToRoute('booking', booking);
    }
  }
});
