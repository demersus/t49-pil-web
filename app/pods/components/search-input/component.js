import Component from '@ember/component';
import Ember from 'ember';

const {
  run: {debounce}
} = Ember;

export default Component.extend({
  value: null,
  onupdate: function(){}, //closure action

  actions: {
    update(value) {
      debounce(this, this.onupdate, value, 300);
    }
  }

});
