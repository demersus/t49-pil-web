import Component from '@ember/component';

export default Component.extend({
  tagName: '',
  trace: false,

  actions: {
    toggleTrace() {
      this.toggleProperty('trace');
    }
  }
});
