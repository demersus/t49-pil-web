import Ember from 'ember';

const {
  Component,
  computed,
  inject: {service},
  get
} = Ember;

export default Component.extend({
  localSettings: service(),
  favorites: computed.reads('localSettings.favorites'),

  actions: {
    remove(bookingNum) {
      get(this, 'localSettings').removeBookingFromFavorites(bookingNum);
    }
  }
});
