import Ember from 'ember';
import Component from '@ember/component';

const {
  get,
  set,
  computed,
  isPresent,
  inject: {service},
  run,
  run: {debounce}
} = Ember;

export default Component.extend({
  store: service(),
  bookingNumber: null,
  openBooking(){}, //closure action
  errorMessage: null,
  hasError: computed.notEmpty('errorMessage'),

  actions: {
    doLookup() {
      const bookingId = get(this, 'bookingNumber');

      if(isPresent(bookingId)) {
        const lookup = get(this, 'store').find('booking', bookingId).then((booking) => {
          get(this, 'openBooking')(booking);
        }).catch((e) => {
          console.error(e);
          run(()=>{
            set(this, 'errorMessage',
              "Could not find booking.  Please check the booking number you entered");
          });
        });

        set(this, 'lookup', lookup);
      }
    },
    clearError() {
      debounce(this, () => {
        set(this, 'errorMessage', null);
      }, 300);
    }
  }
});
