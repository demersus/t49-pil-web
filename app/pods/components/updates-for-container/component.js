import Component from '@ember/component';
import Ember from 'ember';

const {
  get,
  inject: {service},
  computed,
  set
} = Ember;

export default Component.extend({
  store: service(),
  booking: null,
  container: null,
  updates: [],
  updatesSort: ['dateUtc:desc'],

  sortedUpdates: computed.sort('updates', 'updatesSort'),

  isLoading: computed.reads('updates.isPending'),

  didReceiveAttrs() {
    this._super(...arguments);

    const bookingNumber = get(this, 'booking.bookingNumber');
    const containerNumber = get(this, 'container.containerNumber');

    set(this, 'updates',
      get(this, 'store').query('update', {
        booking_number: bookingNumber,
        container_number: containerNumber
      })
    );
  }
});
