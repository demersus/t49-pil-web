import { ActiveModelSerializer } from 'active-model-adapter';

export default ActiveModelSerializer.extend({
  extractId(typeClass, resourceHash) {
    return resourceHash['container_number'];
  }
});
