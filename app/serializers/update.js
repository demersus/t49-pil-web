import { ActiveModelSerializer } from 'active-model-adapter';

export default ActiveModelSerializer.extend({
  extractId(typeClass, data) {
    return `${data.container_number}-${data.date}`;
  }
});
