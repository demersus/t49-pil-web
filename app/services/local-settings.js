/* global localStorage */
import Ember from 'ember';

const {
  get,
  set
} = Ember;

export default Ember.Service.extend({
  favorites: Ember.A(),
  storage: localStorage,

  init() {
    this._super();

    this._load();
  },

  _load() {
    const favorites = get(this, 'storage').getItem('favorites');
    try {
      if(favorites) {
        set(this, 'favorites', Ember.A(JSON.parse(favorites)));
      }
    } catch(e) {
      console.error(e);
    }
  },

  _save() {
    const favorites = get(this, 'favorites');
    get(this, 'storage').setItem('favorites', JSON.stringify(favorites.toArray()));
  },

  addBookingToFavorites(booking_number) {
    const favorites = get(this, 'favorites');
    if(!favorites.includes(booking_number)) {
      favorites.pushObject(booking_number);
      this._save();
    }
  },

  removeBookingFromFavorites(booking_number) {
    get(this, 'favorites').removeObject(booking_number);
    this._save();
  }
});
