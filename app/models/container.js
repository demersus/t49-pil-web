import DS from 'ember-data';

export default DS.Model.extend({
  containerNumber: DS.attr('string'),
  containerType: DS.attr('string'),
  movementType: DS.attr('string'),
  latestEvent: DS.attr('string'),
  date: DS.attr('moment-date'),
  place: DS.attr('string')
});
