import DS from 'ember-data';

import Ember from 'ember';

const {
  computed,
  get
} = Ember;

export default DS.Model.extend({
  date: DS.attr('moment-date'),
  latestEvent: DS.attr('string'),
  place: DS.attr('string'),

  dateUtc: computed('date', function() {
    const date = get(this, 'date');

    if(date) {
      return date.format('x');
    }
  })
});
