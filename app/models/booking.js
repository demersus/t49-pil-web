import DS from 'ember-data';

export default DS.Model.extend({
  bookingNumber: DS.attr('string'),
  steamshipLine: DS.attr('string'),
  placeOfDelivery: DS.attr('string'),
  placeOfReceipt: DS.attr('string'),
  location: DS.attr('string'),
  vessel: DS.attr('string'),
  voyage: DS.attr('string'),
  numberOfContainers: DS.attr('string'),
  arrival: DS.attr('string'),
  delivery: DS.attr('string'),
  nextLocation: DS.attr('string'),
  nextLocationDate: DS.attr('moment-date'),
  containers: DS.hasMany()
});
